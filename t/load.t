# $Id$
BEGIN {
	@classes = qw(Pod::LaTeX::TPR);
	}

use Test::More tests => scalar @classes;
	
foreach my $class ( @classes )
	{
	print "bail out! Could not compile $class" unless use_ok( $class );
	}
